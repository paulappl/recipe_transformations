categories = ['meat',
			  'vegetable',
			  'green',
			  'mushroom',
			  'fruit',
			  'cereal',
			  'oil',
			  'spice',
			  'dairy',
			  'egg',
			  'protein',
			  'healthy',
			  'salty',
			  'sweet',
			  'spicy',
			  'acid',
			  'sauce',
			  'bean',
			  'nut',
			  'broth',
			  'vegetarian',
			  'italian',
			  'japanese',
			  'indian',
			  'healthy',
			  'glutenfree'
			  ]

meat = set(["chicken","bacon","beef","chorizo","dog","duck","frog",
		"game","goat","goose","ham","horse","kidney","liver","lamb","mutton",
		"pepperoni","pork","prosciutto","quail","rabbit","salami","sausage",
		"turkey","veal","venison","ground beef","beef heart","beef tongue",
		"tongue","heart","bison","buffalo","caribou","reindeer","marrow","blood",
		"moose","organ meats","organ","kangaroo","sweetbreads","tripe","turtle",
		"ostrich","pheasant","partridge","squab","cornish game hen","hen", "carp",
		"catfish","salmon","tilapia","grouper","bass","seabass","swordfish","shark","squid","octopus"
		"rock fish","sole","cod","haddock","halibut","anchovy","pike","flounder","lamprey",
		"herring","trout","rainbow trout","mahi-mahi","mahi","perch","pollock","mackerel",
		"tuna","walleye","whitefish","snapper","yellowtail","shrimp","eel","prawn","lobster",
		"crayfish","crab","blue crab","oyster","mussel","snail","abalone","clam","alligator",
		"crocodile","jellyfish","pulled pork","pulled chicken","pork chop","steak","fragois"])

#add unions - bean, legume, meat
protein = set(["tofu","soy","soy bean","edamame","tempeh","miso","natto","tamari","seitan","chicken","bacon","beef","chorizo","dog","duck","frog",
		"game","goat","goose","ham","horse","kidney","liver","lamb","mutton",
		"pepperoni","pork","prosciutto","quail","rabbit","salami","sausage",
		"turkey","veal","venison","ground beef","beef heart","beef tongue",
		"tongue","heart","bison","buffalo","caribou","reindeer","marrow","blood",
		"moose","organ meats","organ","kangaroo","sweetbreads","tripe","turtle",
		"ostrich","pheasant","partridge","squab","cornish game hen","hen", "carp",
		"catfish","salmon","tilapia","grouper","bass","seabass","swordfish","shark","squid","octopus"
		"rock fish","sole","cod","haddock","halibut","anchovy","pike","flounder","lamprey",
		"herring","trout","rainbow trout","mahi-mahi","mahi","perch","pollock","mackerel",
		"tuna","walleye","whitefish","snapper","yellowtail","shrimp","eel","prawn","lobster",
		"crayfish","crab","blue crab","oyster","mussel","snail","abalone","clam","alligator",
		"crocodile","jellyfish","pulled pork","pulled chicken","pork chop","steak","fragois"])

dairy = set(["milk","butter","cream","heavy cream","cheese","yogurt","butterfat","buttermilk",
			"condensed milk","cottage cheese","cream cheese","curd","custard","evaporated milk",
			"frozen custard","frozen yogurt","ice cream","paneer","chhena","khoa","lassi",
			"powedered milk","quark","shrikhand","so","strained yogurt","whipped cream","whey",
			"sour cream","doogh","daigo","chass"])

vegetable = set(["corn", "tomato", "green pepper", "red pepper", "mushroom", "kale", "spinach", "eggplant",
	"asparagus", "artichoke", "beet", "beetroot", "celery", "carrot", "collard green", "daikon",
	"cucumber", "onion", "garlic", "green onion", "spring onion", "leek", "okra", "pea", "broccoli",
	"potato", "pumpkin","olive", "radish", "lettuce", "rhubarb", "shallot", "brussel sprout", "sunchoke",
	"sweet potato", "tomatillo", "zucchini", "yam", "white asparagus", "bok choy", "arugula",
	"cabbage", "dill", "endive", "radicchio", "sorrel", "avocado", "aubergine", "brinjal", "squash",
	"bitter gourd", "butternut squash", "acorn squash", "sweet pepper", "banana pepper", "sweet corn",
	"caper", "cauliflower", "drumstick", "snap pea", "snow pea", "chive", "kohlrabi", "wild garlic", 
	"bamboo", "bamboo shoot", "cassava", "ginger", "parsley", "turnip", "rutabaga", "taro", "nori", 
	"kombu", "hijiki", "ogonori", "wakame", "sea grape", "sea lettuce", "arame", "carola", "gim",
	"mozuku", "aonori", "chard", "swiss chard", "spaghetti squash", "parsnip", "cactus","heart of palm"])

green = set(["kale", "spinach", "celery green", "beet green", "collard green", "leek green", "arugula", "bok choy",
	"cabbage", "endive", "dill", "radicchio", 'parsley', "chive", "wild garlic", "nori", 
	"kombu", "hijiki", "ogonori", "wakame", "sea grape", "sea lettuce", "arame", "carola", "gim",
	"mozuku", "aonori", "chard", "swiss chard"])

oil = set(["coconut oil", "corn oil", "cottonseed oil", "olive oil", "palm oil", "peanut oil", 
	"grapeseed oil", "safflower oil", "sesame oil", "soybean oil", "sunflower oil", "mustard oil",
	"almond oil", "cashew oil", "hazelnut oil", "macadamia oil", 'pine nut oil', "walnut oil", "flaxseed oil",
	"argan oil", "cocoa butter", "hemp oil", "shea butter", "castor oil", "butter",
	"margarine", "earth balance", "ghee" "salted butter", "unsalted butter"])

acid = set(["balsamic vinegar", "apple cider vinegar", "rice vinegar", "sherry", "red wine vinegar", "citric acid",
	"lime juice", "lemon juice", "grapefruit juice", "ascorbic acid", "orange juice", "lemon zest", "zest"
	"orange zest", "tamarind", "lemongrass", "preserved lemon", "wine","white vinegar"])

egg = set(["egg", "yolk", "white","egg white","egg yolk", "egg replacer", "tapioca powder"])

spices = set(["ajwain", "allspice", "anise", "aniseed", "asafoetida", "sweet basil", "lemon basil",
	"thai basil", "holy basil", "basil", "bay leaf", "black mustard", "brown mustard", "mustard", "caraway"
	"cardamom", "carob", "catnip", "cayenne", "cayenne pepper", "cassia", "celery leaf", "celery seed", "celery",
	"coriander leaf", "coriander seed", "coriander", "chervil", "chicory", "chili pepper", "chive",
	"cicely", "cilantro", "cinnamon", "white cinnamon", "clove", "cumin", "curry leaf", "dill", "dill seed",
	"elderflower", "epazote", "fennel", "fenugreek", "fingerroot", "galangal", "garlic", "elephant garlic",
	"ginger", "horseradish", "hyssop", "jasmine", "juniper", "jimbu", "kaffir lime", "kaffir lime leaves", "kaffir lime leaf",
	"kaffir lime lea", "kawakawa", "lavender", "lemon balm", "lemongrass", "lemon myrtle", "lemon verbena",
	'licorice', "lime flower", "lovage", "mace", "marjoram", "mastic", "mint", "white mustard", "nasturtium",
	"nutmeg", "neem", "oregano", "paprika", "parsley", "paracress", "black pepper", "white pepper", "green pepper",
	"peppermint", "rosemary", "rue", "safflower", "saffron", "sage", "sassafras", "savory", "sesame",
	"shiso", "sorrel", "spearmint", "star anise", "sumac", "spikenard", "tarragon", "thyme", "turmeric",
	"vanilla", "wasabi", "watercress", "wintergreen", "woodruff", "wormwood", "za'atar","salt"])

italian = set(["chicken","bacon","beef","game","ham","liver","kidney",
			   "pepperoni","pork","prosciutto","rabbit",'salami','sausage',
			   'veal','venison','ground beef','organ meats','organ','sweetbreads',
			   'tripe','pheasant','tilapia','squid','octopus','cuttlefish',
			   'anchovy','anchovies','clams','mussels','mullet','flounder'
			   ,'shrimp',
			   "milk","butter","cream","heavy cream","cheese",
			   "tomato", "green pepper", "red pepper", "mushroom",
			   "spinach", "eggplant", "asparagus", "artichoke",
			   "cucumber", "onion", "garlic", "green onion", "spring onion", "leek",
			   "broccoli","potato","zucchini","white asparagus","arugula",
			   "banana pepper","radish", "lettuce", "shallot", "brussel sprout",
			   "cabbage", "dill", "endive", "radicchio", "sorrel","caper","chive","wild garlic",
			   "ginger","parsley", "turnip", "rutabaga","parsnip","heart of palm","olive",
			   "spinach", "celery green", "beet green", "collard green", "leek green",
			   "dill","parsley",
			   "olive oil", "palm oil","grapeseed oil","mustard oil","almond oil",'pine nut oil',
			   "balsamic vinegar","sherry","red wine vinegar","wine","lime juice", "lemon juice"
			   ,"grapefruit juice","ascorbic acid","orange juice", "lemon zest", "zest","orange zest",
			   "preserved lemon",
			   "egg", "yolk", "white","egg white","egg yolk",
			   "anise", "aniseed","sweet basil", "lemon basil","holy basil", "basil"
			   ,"bay leaf","black mustard", "brown mustard", "mustard", "caraway,
			   "cayenne", "cayenne pepper","celery leaf", "celery seed", "celery",
			   "coriander leaf", "coriander seed", "coriander", "chervil", "chicory",
			   "chive","cicely", "cilantro", "cinnamon", "white cinnamon", "clove",
			   "dill", "dill seed","elderflower","fennel","garlic","hyssop",'licorice',
			   "mace","mastic", "mint", "white mustard","peppermint", "rosemary","sage",
			   "sesame","spearmint", "thyme","vanilla","watercress"])

indian = set(["egg","lamb", "mutton", "chicken", "pomfret","prawn","hilsa","shrimp","curd","milk", 
	"condensed milk", "sour cream", "kulfi","yogurt","butterfat", "cottage cheese","lassi", "paneer",
	"ghee","clarified butter", "khoa","buttermilk","mustard oil", "coconut oil", "papad", "semolina", 
	"cardamom","raisins","saffron", "pistachios","almonds","lovage", "pomegranate","bay leaf","turmeric",
	"cinnamon", "coriander", "cloves", "cumin","curry leaves","fenugreek","garam masala","curry masala",
	"black pepper", "chilli", "saffron","fennel seed","tamarind","mustard","black mustard", "white mustard",
	"nigella", "ajwain", "bay leaf", "black mustard",  "caraway","cardamom", "coriander leaf", "coriander seed", 
	"chili pepper", "cilantro", "white cinnamon", "clove", "cumin", "curry leaf", "dill", "dill seed",
	"epazote", "fennel", "fennel seed","ginger", "jimbu", "kaffir lime", "kaffir lime leaves", "kaffir lime leaf",
	"kaffir lime lea",  "lime flower","mint", "white mustard","nutmeg", "neem", "black pepper", "white pepper",
	"rue", "safflower", "saffron", "turmeric","asafetida","potato","coconut","gourd","pumpkin", "tomato", 
	"green pepper", "red pepper", "mushroom","spinach", "eggplant","asparagus", "beet", "beetroot","carrot",
	"cucumber", "onion", "garlic","okra", "pea","radish", "sweet potato", "cabbage", "brinjal", "squash",
	"bitter gourd", "butternut squash","acorn squash",  "cauliflower", "drumstick", "kohlrabi", "wild garlic", 
	"ginger", "parsley", "turnip", "lime juice", "lemon juice","dal","masoor dal","mung dal","chana",
	"toor dal","urad dal"])
